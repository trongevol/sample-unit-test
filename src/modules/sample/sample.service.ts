import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Op, QueryTypes, Sequelize } from 'sequelize';
import { SentryService } from '../../third-parties/sentry/sentry.service';
import { UserModel } from '../../database/models';
import { UserService } from '../users/user.service';
import { NestApplicationContextOptions } from '@nestjs/common/interfaces/nest-application-context-options.interface';
import { resolveConfigFile } from 'prettier';

@Injectable()
export class SampleService {
  constructor(
    @InjectModel(UserModel)
    private userModel: typeof UserModel,
    private userService: UserService,
    private sequelize: Sequelize,
    private sentryService: SentryService,
  ) {}

  async getAllUsersViaModel() {
    return await this.userModel.findAll();
  }

  async getAllUsersViaUserService() {
    return await this.userService.getAllUsers();
  }

  async getAllUsersByNameSortByAgeAsc(name: string) {
    return await this.userModel.findAll({
      where: {
        name: { [Op.iLike]: `%${name}%` },
      },
      order: [['age', 'asc']],
    });
  }

  async getAllUserInAgeRangeByRawQuery(min: number, max: number) {
    const query = `SELECT * FROM user WHERE age BETWEEN :min AND :max`;
    const result = await this.sequelize.query(query, {
      replacements: {
        min: min,
        max: max,
      },
      type: QueryTypes.SELECT,
      raw: true,
    });

    return result;
  }

  explicitTypeOfParameter(message: string, sentryService: SentryService) {
    sentryService.captureMessage(message);
  }

  async complexInserUserFunction(
    firstName: string,
    lastName: string,
    age: number,
  ) {
    const name = this.createNameFromFirstAndLast(firstName, lastName);
    const isExist = await this.checkIfNameExisted(name);
    if (isExist) {
      throw new Error('The name is existed');
    } else {
      try {
        await this.userModel.create({
          name: name,
          age: age,
        });
        return true;
      } catch (exception) {
        this.sentryService.captureException(exception);
        return false;
      }
    }
  }

  createNameFromFirstAndLast(firstName, lastName) {
    return firstName + ' ' + lastName;
  }

  async checkIfNameExisted(name) {
    const rs = await this.userModel.findOne({
      where: { name: name },
    });
    return rs !== null;
  }

  async createManyUsersUnmanagedTransaction(
    users: { name: string; age: number }[],
  ) {
    const unmanagedTrx = await this.sequelize.transaction();
    try {
      for (const i in users) {
        await this.userModel.create(users[i], { transaction: unmanagedTrx });
      }
      unmanagedTrx.commit();
      return true;
    } catch (exception) {
      this.sentryService.captureException(exception);
      unmanagedTrx.rollback();
      return false;
    }
  }
}

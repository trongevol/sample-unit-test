import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/sequelize';
import { UserModel } from '../../../database/models';
import { SampleService } from '../sample.service';
import { UserService } from '../../users/user.service';
import { Sequelize } from 'sequelize';
import { SentryService } from '../../../third-parties/sentry/sentry.service';

const mockUserModel = {
  findAll: jest.fn(), // stub for almost test cases in this file
  findOne: jest.fn(), // stub for test complexInserUserFunction function
  create: jest.fn(), // stub for test complexInserUserFunction function
};

const mockUserService = {
  getAllUsers: jest.fn(),
};

const mockTransaction = {
  commit: jest.fn(),
  rollback: jest.fn(),
};

const mockSequelize = {
  query: jest.fn(),
  transaction: () => {
    return mockTransaction;
  },
};

const mockSentryService = {
  captureMessage: jest.fn(),
  captureException: jest.fn(),
};

describe('SampleService', () => {
  let sampleService: SampleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(UserModel),
          useValue: mockUserModel,
        },
        { provide: UserService, useValue: mockUserService },
        { provide: Sequelize, useValue: mockSequelize },
        { provide: SentryService, useValue: mockSentryService },
        SampleService,
      ],
    }).compile();

    sampleService = module.get<SampleService>(SampleService);
  });

  describe('createManyUsersUnmanagedTransaction', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should commit', async () => {
      const usersInputData = [
        { name: 'Iron Man', age: 50 },
        { name: 'Captain American', age: 90 },
        { name: 'Black Panther', age: 30 },
      ];

      mockUserModel.create.mockResolvedValue(true);
      const result = await sampleService.createManyUsersUnmanagedTransaction(
        usersInputData,
      );

      expect(result).toBe(true);
      expect(mockUserModel.create).toBeCalledTimes(usersInputData.length);

      // get argruments of userModel.create function of each calls then expect it
      // all argruments of a function call are put in an array
      expect(mockUserModel.create.mock.calls[0]).toEqual([
        { name: 'Iron Man', age: 50 },
        { transaction: mockTransaction },
      ]);
      expect(mockUserModel.create.mock.calls[1]).toEqual([
        { name: 'Captain American', age: 90 },
        { transaction: mockTransaction },
      ]);
      expect(mockUserModel.create.mock.calls[2]).toEqual([
        { name: 'Black Panther', age: 30 },
        { transaction: mockTransaction },
      ]);
      expect(mockTransaction.commit).toBeCalled();
    });

    it('should rollback', async () => {
      const usersInputData = [
        { name: 'Iron Man', age: 50 },
        { name: 'Captain American', age: 90 },
        { name: 'Black Panther', age: 30 },
      ];

      mockUserModel.create.mockResolvedValueOnce(true);
      mockUserModel.create.mockRejectedValueOnce(new Error('Duplicate name'));
      const result = await sampleService.createManyUsersUnmanagedTransaction(
        usersInputData,
      );
      expect(result).toBe(false);
      expect(mockUserModel.create).toBeCalledTimes(2); // not call 3 times because it will failed at 2nd

      // get argruments of userModel.create function of each calls then expect it
      // all argruments of a function call are put in an array
      expect(mockUserModel.create.mock.calls[0]).toEqual([
        { name: 'Iron Man', age: 50 },
        { transaction: mockTransaction },
      ]);
      expect(mockUserModel.create.mock.calls[1]).toEqual([
        { name: 'Captain American', age: 90 },
        { transaction: mockTransaction },
      ]);

      expect(mockTransaction.commit).not.toBeCalled();
      expect(mockTransaction.rollback).toBeCalled();

      expect(mockSentryService.captureException).toBeCalledWith(
        new Error('Duplicate name'),
      );
    });
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/sequelize';
import { UserModel } from '../../../database/models';
import { SampleService } from '../sample.service';
import { UserService } from '../../users/user.service';
import { Op, QueryTypes } from 'sequelize';
import { Sequelize } from 'sequelize';
import { SentryService } from '../../../third-parties/sentry/sentry.service';

const mockUserModel = {
  findAll: jest.fn(), // stub for almost test cases in this file
  findOne: jest.fn(), // stub for test complexInserUserFunction function
  create: jest.fn(), // stub for test complexInserUserFunction function
};

const mockUserService = {
  getAllUsers: jest.fn(),
};

const mockSequelize = {
  query: jest.fn(),
};

const mockSentryService = {
  captureMessage: jest.fn(),
  captureException: jest.fn(),
};

describe('SampleService', () => {
  let sampleService: SampleService;
  let sentryService: SentryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(UserModel),
          useValue: mockUserModel,
        },
        { provide: UserService, useValue: mockUserService },
        { provide: Sequelize, useValue: mockSequelize },
        { provide: SentryService, useValue: mockSentryService },
        SampleService,
      ],
    }).compile();

    sampleService = module.get<SampleService>(SampleService);

    // this variable is use to pass to functions that require parameter must be SentryService type
    // see the test case of explicitTypeOfParameter function
    sentryService = module.get<SentryService>(SentryService);
  });

  it('should be defined', () => {
    expect(sampleService).toBeDefined();
  });

  describe('getAllUsersViaModel', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return data', async () => {
      const fakeUserData = [
        { name: 'John', age: 28 },
        { name: 'Mary', age: 25 },
      ];
      mockUserModel.findAll.mockResolvedValue(fakeUserData);
      const rs = await sampleService.getAllUsersViaModel();
      expect(rs).toEqual(fakeUserData);
    });
  });

  describe('getAllUsersViaUserService', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return data', async () => {
      const fakeUserData = [
        { name: 'Peter', age: 27 },
        { name: 'Jane', age: 22 },
      ];
      mockUserService.getAllUsers.mockResolvedValue(fakeUserData);
      const rs = await sampleService.getAllUsersViaUserService();
      expect(rs).toEqual(fakeUserData);
    });
  });

  describe('getAllUsersByNameSortByAgeAsc', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return sorted data', async () => {
      const key = 'a';

      // this data is not sorted by age asc
      const fakeUserData = [
        { name: 'Paul', age: 23 },
        { name: 'May', age: 21 },
      ];
      mockUserModel.findAll.mockResolvedValue(fakeUserData);
      const result = await sampleService.getAllUsersByNameSortByAgeAsc(key);
      expect(result).toEqual(fakeUserData);

      // because the result not improve that queried data is sorted
      // so we must expect userModel.findAll is called with key = a and order by age asc
      expect(mockUserModel.findAll).toBeCalledWith({
        where: {
          name: { [Op.iLike]: '%a%' },
        },
        order: [['age', 'asc']],
      });
    });
  });

  describe('getAllUserInAgeRangeByRawQuery', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return data and correctly parameters', async () => {
      const min = 20,
        max = 30;

      // this data is not sorted by age asc
      const fakeUserData = [
        { name: 'Paul', age: 23 },
        { name: 'May', age: 21 },
      ];
      mockSequelize.query.mockResolvedValue(fakeUserData);
      const result = await sampleService.getAllUserInAgeRangeByRawQuery(
        min,
        max,
      );
      expect(result).toEqual(fakeUserData);

      expect(mockSequelize.query).toBeCalledWith(
        'SELECT * FROM user WHERE age BETWEEN :min AND :max',
        {
          replacements: {
            min: min,
            max: max,
          },
          type: QueryTypes.SELECT,
          raw: true,
        },
      );
    });
  });

  describe('explicitTypeOfParameter', () => {
    it('should call sentryService.captureMessage', () => {
      sampleService.explicitTypeOfParameter('abc', sentryService);
      expect(sentryService.captureMessage).toBeCalledWith('abc');
    });
  });

  describe('complexInserUserFunction', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return true if name not exist', async () => {
      // function createNameFromFirstAndLast just only process a logic, so we only need SPY it, not test it
      const createNameFromFirstAndLastSpy = jest.spyOn(
        sampleService,
        'createNameFromFirstAndLast',
      );

      // same reason for checkIfNameExisted
      const checkIfNameExistedSpy = jest.spyOn(
        sampleService,
        'checkIfNameExisted',
      );

      // function checkIfNameExisted calls a function from model to query data,
      // so we need STUB the findOne funciton and force it to return a fake data
      const fakeUserData = null;
      mockUserModel.findOne.mockResolvedValue(fakeUserData);

      const result = await sampleService.complexInserUserFunction(
        'Iron',
        'Man',
        50,
      );
      expect(result).toBe(true);
      expect(createNameFromFirstAndLastSpy).toBeCalledWith('Iron', 'Man');
      // we don't really need expect mockUserModel.findOne because it is called inside checkIfNameExisted
      // so we should ignore it and let it be tested when we test the checkIfNameExisted function
      expect(checkIfNameExistedSpy).toBeCalledWith('Iron Man');
      expect(mockUserModel.create).toBeCalledWith({
        name: 'Iron Man',
        age: 50,
      });
    });

    it('should throw exception if name existed', async () => {
      const createNameFromFirstAndLastSpy = jest.spyOn(
        sampleService,
        'createNameFromFirstAndLast',
      );

      // same reason for checkIfNameExisted
      const checkIfNameExistedSpy = jest.spyOn(
        sampleService,
        'checkIfNameExisted',
      );

      // fake a data for the findOne function
      const fakeUserData = { name: 'Captain American', age: 50 };
      mockUserModel.findOne.mockResolvedValue(fakeUserData);

      // because the function will be throw exception so we must put expectation in catch block
      try {
        await sampleService.complexInserUserFunction('Iron', 'Man', 50);
      } catch (exception) {
        expect(createNameFromFirstAndLastSpy).toBeCalledWith('Iron', 'Man');
        expect(checkIfNameExistedSpy).toBeCalledWith('Iron Man');
        expect(mockUserModel.create).not.toBeCalled();
        expect(exception).toEqual(new Error('The name is existed'));
      }
    });

    it('show return false and send exception to Sentry when userModel.create throw exception', async () => {
      // must still fake a data for the findOne function to pass logic check user existed or not
      const fakeUserData = null;
      mockUserModel.findOne.mockResolvedValue(fakeUserData);

      const fakeError = new Error('Age column not found');
      mockUserModel.create.mockRejectedValue(fakeError);
      const result = await sampleService.complexInserUserFunction(
        'Iron',
        'Man',
        50,
      );
      expect(result).toBe(false);
      // at here, we only need expect mockSentryService not sentryService
      // because mockSentryService is object we injected to sampleService as dependency
      expect(mockSentryService.captureException).toBeCalledWith(fakeError);
    });

    it('test empty', () => {
      const a = 1;
      expect(a).toBe(1);
    });
  });
});

import { Module } from '@nestjs/common';
import { SampleService } from './sample.service';
import { AdvancedSampleService } from './advanced-sample.service';

@Module({
  providers: [SampleService, AdvancedSampleService],
})
export class SampleModule {}

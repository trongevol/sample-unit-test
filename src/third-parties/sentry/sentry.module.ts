import { Global, Module } from '@nestjs/common';
import { SentryService } from './sentry.service';

@Global()
@Module({
  imports: [],
  providers: [SentryService],
  exports: [SentryService],
})
export class SentryModule {}

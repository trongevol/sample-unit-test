import { Injectable } from '@nestjs/common';
import * as Sentry from '@sentry/node';

@Injectable()
export class SentryService {
  captureException(exception) {
    Sentry.captureException(exception);
  }

  captureMessage(message) {
    Sentry.captureMessage(message);
  }

  captureEvent(event: Sentry.Event) {
    Sentry.captureEvent(event);
  }
}

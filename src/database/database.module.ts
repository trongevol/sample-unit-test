import models from './models';
import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

@Module({
  imports: [
    SequelizeModule.forRootAsync({
      useFactory: () => {
        return {
          models: models,
          operatorsAliases: null,
          dialect: 'postgres',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: 'password',
          database: 'nest',
          define: {
            paranoid: true,
            timestamps: true,
            underscored: true,
          },
        };
      },
    }),
    SequelizeModule.forFeature(models),
  ],
  exports: [SequelizeModule],
})
export class DatabaseModule {}

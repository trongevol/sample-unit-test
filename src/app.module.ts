import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { SampleModule } from './modules/sample/sample.module';
import { SentryModule } from './third-parties/sentry/sentry.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [DatabaseModule, SentryModule, SampleModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
